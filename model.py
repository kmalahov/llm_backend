import torch
import sys
import os
from tqdm import tqdm
# from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.output_parsers import ResponseSchema
from langchain.output_parsers import StructuredOutputParser
from langchain.prompts import ChatPromptTemplate
from langchain.chains import LLMChain, MapReduceDocumentsChain, ReduceDocumentsChain, StuffDocumentsChain
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.chains.summarize import load_summarize_chain
from langchain.docstore.document import Document
from tqdm import tqdm
import transformers
import pandas as pd
import numpy as np
import langchain_community
# from langchain.llms import LlamaCpp
from langchain.prompts import PromptTemplate
from langchain.output_parsers import PydanticOutputParser
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from huggingface_hub import hf_hub_download
from langchain_community.llms import HuggingFacePipeline
from torch import bfloat16, float32
import transformers

# Промпты по умолчанию
# prompts = {
#     "question": "You should create a quize: QUESTION and 4 ANSWERS. You need use a topic from '{text}' in QUESTION part. Words from QUESTION should not be repeated in ANSWER. Answer options should be simple. Question should be interesting. You should generate one example of quize. Don't generate another text except quize text",
#     "answer": "You should generate the question and 4 answers options for quize. Sentence: '{text}', should be into one of answer option. Answer options should be simple. Question should be interesting. You should generate one example of quize"
# }


async def load_intel_model():
    sample_generation = True
    model_name = 'Intel/neural-chat-7b-v3-1'

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        pretrained_model_name_or_path=model_name,
    )
    bnb_config = transformers.BitsAndBytesConfig(
        load_in_8bit=True,
        load_in_4bit=False,
        bnb_8bit_quant_type='nf8',
        bnb_4bit_use_double_quant=False,
        bnb_8bit_use_double_quant=True,
        bnb_8bit_compute_dtype=bfloat16
    )
    model_config = transformers.AutoConfig.from_pretrained(
        pretrained_model_name_or_path=model_name,
    )
    model = transformers.AutoModelForCausalLM.from_pretrained(
        pretrained_model_name_or_path=model_name,
        resume_download=True,
        trust_remote_code=True,
        config=model_config,
        quantization_config=bnb_config,
        device_map='auto'
    )
    model.eval()
    pipeline_args = {}
    if sample_generation:
        pipeline_args['top_k'] = 20
        pipeline_args['top_p'] = 0.95
        pipeline_args['temperature'] = 0.1
    generate_text = transformers.pipeline(
        model=model,
        tokenizer=tokenizer,
        return_full_text=True,
        task='text-generation',
        do_sample=sample_generation,
        max_new_tokens=1024,
        repetition_penalty=1.1,
        **pipeline_args
    )
    generate_text.model.config.pad_token_id = generate_text.model.config.eos_token_id

    llm = HuggingFacePipeline(pipeline=generate_text)

    return llm


# async def get_prompt(question: str, prompt_type: int, difficulty: int):
#     match prompt_type:
#         case 1:
#             message_format = """
#                     Следуй шаблону:\n
#                     SLIDE BEGIN\n
#                     CONTENT: {text max 100 words}\n
#                     SLIDE END\n"""
#             message = """Сгенерируй слайд для презентации. Используй только правдивую информацию.
#             Тема {question} со сложностью {difficulty}.
#             Используй только правдивую информацию. {message_format}"""
#         case 2:
#             message_format = """
#                     Следуй шаблону:\n
#                     QUESTION BEGIN\n
#                     QUESTION: {text max 100 words}\n
#                     OPTION_1: {text}\n
#                     OPTION_2: {text}\n
#                     OPTION_3: {text}\n
#                     OPTION_4: {text}\n
#                     RIGHT_OPTION_NUMBER: {number}\n
#                     QUESTION END\n"""
#             message = """Представь, что сложность можно распределить от 1 до 10.
#             Сгенерируй вопрос для викторины по теме {question} со сложностью {difficulty}.
#             Используй только правдивую информацию. {message_format}"""
#         case 3:
#             message_format = """
#                     Следуй шаблону:\n
#                     QUESTION BEGIN\n
#                     QUESTION: {text max 100 words}\n
#                     RIGHT_ANSWER: {text}\n
#                     QUESTION END\n"""
#             message = """Представь, что сложность можно распределить от 1 до 10.
#             Создай вопрос для викторины по теме {question} со сложностью {difficulty}.
#             Тип вопроса открытый вопрос: Создай открытый вопрос на тему {question}, который подразумевает единственный верный ответ.
#             Вопрос должен быть достаточно конкретным, чтобы быть связанным с заданной темой. {message_format}
#             """
#         case 4:
#             message_format = """
#                     Четко следуй шаблону, ничего не меняй:\n
#                     QUESTION BEGIN\n
#                     QUESTION: (Установите соответствие...) ИЛИ (Соотнесите...) {text max 100 words}\n
#                     OPTION_1: {text}\n
#                     OPTION_2: {text}\n
#                     OPTION_3: {text}\n
#                     OPTION_4: {text}\n
#                     MATCH_1: {text}\n
#                     MATCH_2: {text}\n
#                     MATCH_3: {text}\n
#                     MATCH_4: {text}\n
#                     QUESTION END\n"""
#             message = """Представь, что сложность можно распределить от 1 до 10.
#             Создай вопрос для викторины по теме {question} со сложностью {difficulty}.
#             Необходимо, чтобы участники установили соответствие.
#             Используй только правдивую информацию. {message_format}"""
#         case 5:
#             message_format = """
#                     Четко следуй шаблону, ничего не меняй:\n
#                     QUESTION BEGIN\n
#                     QUESTION: (Расположите в правильном порядке...) ИЛИ (Расположите от меньшего к большему...) ИЛИ (Расположите в порядке возрастания...) {text max 100 words}\n
#                     OPTION_1: {text}\n
#                     OPTION_2: {text}\n
#                     OPTION_3: {text}\n
#                     OPTION_4: {text}\n
#                     RIGHT_OPTIONS_ORDER: {numbers separated by comma}\n
#                     QUESTION END\n"""
#             message = """Представь, что сложность можно распределить от 1 до 10.
#             Создай вопрос для викторины по теме {question} со сложностью {difficulty}.
#             Тип вопроса расположение вариантов в правильном порядке.
#             Используй только правдивую информацию. {message_format}
#             """
#         case 6:
#             message_format = """
#                     Четко следуй шаблону, ничего не меняй:\n
#                     QUESTION BEGIN\n
#                     QUESTION: {text max 100 words}\n
#                     RIGHT_ANSWER: {text}\n
#                     QUESTION END\n"""
#             message = """Представь, что сложность можно распределить от 1 до 10.
#             Создай вопрос для викторины по теме {question} со сложностью {difficulty}.
#             Тип вопроса на да или нет. Разработай вопрос,
#             где участники должны выбрать ДА или НЕТ.
#             Это может быть любой однозначный вопрос,
#             связанная с темой {question}. {message_format}
#             """
#     formatted_prompt = message.format(question=question, difficulty=difficulty, message_format=message_format)
#     print(formatted_prompt)
#     return formatted_prompt


async def get_prompt(question: str, prompt_type: int, difficulty: int):
    match prompt_type:
        case 1:
            formatted_prompt = """Сгенерируй слайд для презентации. Используй только правдивую информацию.
            Тема {question} со сложностью {difficulty}. 
            Используй только правдивую информацию. 
            Следуй шаблону:\n
            SLIDE BEGIN\n
            CONTENT: {text max 100 words}\n
            SLIDE END\n"""
        case 2:
            formatted_prompt = """Представь, что сложность можно распределить от 1 до 10. 
            Сгенерируй вопрос для викторины по теме {question} со сложностью 2. 
            Используй только правдивую информацию.
            Следуй шаблону:\n
            QUESTION BEGIN\n
            QUESTION: {{text max 100 words}}\n
            OPTION_1: {{text}}\n
            OPTION_2: {{text}}\n
            OPTION_3: {{text}}\n
            OPTION_4: {{text}}\n
            RIGHT_OPTION_NUMBER: {{number}}\n
            QUESTION END\n"""
        case 3:
            formatted_prompt = """Представь, что сложность можно распределить от 1 до 10. 
            Создай вопрос для викторины по теме {question} со сложностью {difficulty}. 
            Тип вопроса открытый вопрос: Создай открытый вопрос на тему {question}, который подразумевает единственный верный ответ. 
            Вопрос должен быть достаточно конкретным, чтобы быть связанным с заданной темой.
            Следуй шаблону:\n
            QUESTION BEGIN\n
            QUESTION: {text max 100 words}\n
            RIGHT_ANSWER: {text}\n
            QUESTION END\n
            """
        case 4:
            formatted_prompt = """Представь, что сложность можно распределить от 1 до 10. 
            Создай вопрос для викторины по теме {question} со сложностью {difficulty}.
            Необходимо, чтобы участники установили соответствие.
            Используй только правдивую информацию.
            Четко следуй шаблону, ничего не меняй:\n
            QUESTION BEGIN\n
            QUESTION: (Установите соответствие...) ИЛИ (Соотнесите...) {text max 100 words}\n
            OPTION_1: {text}\n
            OPTION_2: {text}\n
            OPTION_3: {text}\n
            OPTION_4: {text}\n
            MATCH_1: {text}\n
            MATCH_2: {text}\n
            MATCH_3: {text}\n
            MATCH_4: {text}\n
            QUESTION END\n"""
        case 5:
            formatted_prompt = """Представь, что сложность можно распределить от 1 до 10. 
            Создай вопрос для викторины по теме {question} со сложностью {difficulty}. 
            Тип вопроса расположение вариантов в правильном порядке.
            Используй только правдивую информацию.
            Четко следуй шаблону, ничего не меняй:\n
            QUESTION BEGIN\n
            QUESTION: (Расположите в правильном порядке...) ИЛИ (Расположите от меньшего к большему...) ИЛИ (Расположите в порядке возрастания...) {text max 100 words}\n
            OPTION_1: {text}\n
            OPTION_2: {text}\n
            OPTION_3: {text}\n
            OPTION_4: {text}\n
            RIGHT_OPTIONS_ORDER: {numbers separated by comma}\n
            QUESTION END\n
            """
        case 6:
            formatted_prompt = """Представь, что сложность можно распределить от 1 до 10. 
            Создай вопрос для викторины по теме {question} со сложностью {difficulty}. 
            Тип вопроса на да или нет. Разработай вопрос, 
            где участники должны выбрать ДА или НЕТ.
            Это может быть любой однозначный вопрос, 
            связанная с темой {question}.
            Четко следуй шаблону, ничего не меняй:\n
            QUESTION BEGIN\n
            QUESTION: {text max 100 words}\n
            RIGHT_ANSWER: {text}\n
            QUESTION END\n
            """
    print(formatted_prompt)
    return formatted_prompt


async def generate_text(input_sentence: str, question: str, llm):
    prompt = PromptTemplate(
        input_variables=['question'],
        template=input_sentence
    )
    chain = LLMChain(llm=llm, prompt=prompt)
    response = chain.run(question)

    return response
