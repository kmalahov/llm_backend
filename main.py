from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel
import uvicorn
import model
from model import load_intel_model

app = FastAPI()


# Загружаем модель при запуске приложения
@app.on_event("startup")
async def startup_event():
    global llm
    llm = await load_intel_model()


class RequestModel(BaseModel):
    input_sentence: str


class PromptUpdateRequest(BaseModel):
    question_prompt: str
    answer_prompt: str


@app.get("/health-check")
async def root():
    return {"message": "Working!"}


@app.get("/prompts/")
async def get_prompts(question: str, prompt_type: int, difficulty: int):
    prompt = await model.get_prompt(question, prompt_type, difficulty)
    return prompt


@app.put("/prompts/")
async def update_prompts(access_token: str, request: PromptUpdateRequest):
    # model.prompts["question"] = request.question_prompt
    # model.prompts["answer"] = request.answer_prompt
    # return {"message": "Prompts updated successfully"}
    pass


@app.post("/generate/")
async def generate_text(question: str, prompt_type: int, difficulty: int, access_token: str):
    try:
        quest_prompt = await model.get_prompt(question, prompt_type, difficulty)
        question_dict = await model.generate_text(quest_prompt, question, llm)
        return question_dict
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000, reload=True)

